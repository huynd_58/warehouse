package models;

import play.db.ebean.Model;

import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Entity;

/**
 * Created by Huy on 2/28/2015.
 */
@Entity
public class StockItem extends Model{
    @Id
    public Long id;

    @ManyToOne
    public Warehouse warehouse;

    @ManyToOne
    public Product product;

    public Long quantity;

    public static Finder<Long, StockItem> find = new Finder<>(Long.class, StockItem.class);

    public String toString(){
        return String.format("StockItem %d - %d x product %s", id, quantity, product == null ? null : product.id);
    }
}
