package models;

import play.db.ebean.Model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;

/**
 * Created by Huy on 2/28/2015.
 */
@Entity
public class Warehouse extends Model{
    @Id
    public Long id;

    public String name;

    @OneToOne
    public Address address;

    @OneToMany(mappedBy = "warehouse")
    public List<StockItem> stock = new ArrayList<>();

    public static Finder<Long, Tag> find = new Finder<>(Long.class, Tag.class);

    public String toString(){
        return name;
    }
}
