package controllers;

import models.StockItem;
import models.Tag;
import play.mvc.*;
import java.util.List;

/**
 * Created by Huy on 3/3/2015.
 */
public class StockItems extends Controller {
    public static Result index() {
        List<StockItem> items = StockItem.find
                .where()
                .ge("quantity", 300)
                .orderBy("quantity desc")
                .setMaxRows(5)
                .findList();
        return ok(items.toString());
    }
}
